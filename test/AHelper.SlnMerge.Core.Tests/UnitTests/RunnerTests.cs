﻿using CommandLine;
using System;
using System.Linq;
using Xunit;

namespace AHelper.SlnMerge.Core.Tests.UnitTests;

public class RunnerTests
{
    [Fact]
    public void TestEmptyArgs()
    {
        var result = new Parser().ParseArguments<RunnerOptions>(Array.Empty<string>());
        Assert.Single(result.Errors);
        Assert.IsType<MissingRequiredOptionError>(result.Errors.First());
    }

    [Fact]
    public void TestProjects()
    {
        var result = new Parser().ParseArguments<RunnerOptions>(new[] { "A", "B", "C" });
        Assert.Empty(result.Errors);
    }

    [Fact]
    public void TestProperties()
    {
        var result = new Parser().ParseArguments<RunnerOptions>(new[] { "A", "--property", "key=value" });
        Assert.Empty(result.Errors);
        Assert.Single(result.Value.Properties);
        Assert.Equal("value", result.Value.Properties["key"]);
    }

    [Fact]
    public void TestHelp()
    {
        var result = new Parser().ParseArguments<RunnerOptions>(new[] { "--help" });
        Assert.Single(result.Errors);
        Assert.IsType<HelpRequestedError>(result.Errors.First());
    }
}
