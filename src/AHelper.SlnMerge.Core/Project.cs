using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Microsoft.Build.Construction;
using Microsoft.Build.Evaluation;

namespace AHelper.SlnMerge.Core;

public class Reference
{
    public IList<string> Frameworks { get; set; } = new List<string>();
    public string Include { get; set; }
    public string? Origin { get; set; }

    public Reference(string include)
    {
        Include = include;
    }
}

public abstract class Change
{
    public ChangeType ChangeType { get; }
    public string Framework { get; }

    protected Change(string framework, ChangeType changeType)
    {
        Framework = framework;
        ChangeType = changeType;
    }
}

public class VersionChange : Change
{
    public VersionChange(ChangeType changeType)
        : base(string.Empty, changeType)
    { }
}

public class ProjectChange : Change
{
    public Project Project { get; }

    public ProjectChange(Project project, string framework, ChangeType changeType)
        : base(framework, changeType)
    {
        Project = project;
    }
}

public class ReferenceChange : Change
{
    public Reference Reference { get; set; }

    public ReferenceChange(Reference reference, string framework, ChangeType changeType)
        : base(framework, changeType)
    {
        Reference = reference;
    }
}

public class Project
{
    public string PackageId { get; }
    public IList<Reference> PackageReferences { get; }
    public IList<Reference> ProjectReferences { get; }
    public IList<Reference> FrameworkReferences { get; }
    public IList<string> TargetFrameworks { get; set; }
    public string Filepath { get; }
    public List<Change> Changes { get; } = new List<Change>();
    public bool IsLegacy { get; }

    private readonly IOutputWriter _outputWriter;
    private static readonly string[] _arrayOfEmptyString = new[] { "" };

    private Project(string filepath,
                    string packageId,
                    IList<Reference> packageReferences,
                    IList<Reference> projectReferences,
                    IList<Reference> frameworkReferences,
                    IList<string> targetFrameworks,
                    IOutputWriter outputWriter,
                    bool isLegacy)
    {
        Filepath = filepath;
        PackageId = packageId;
        PackageReferences = packageReferences;
        ProjectReferences = projectReferences;
        FrameworkReferences = frameworkReferences;
        TargetFrameworks = targetFrameworks;
        _outputWriter = outputWriter;
        IsLegacy = isLegacy;
    }

    public static async Task<Project> CreateAsync(string filepath, Solution parent, IOutputWriter outputWriter)
    {
        filepath = Path.GetFullPath(filepath, Path.GetDirectoryName(parent.Filepath)!);

        if (!File.Exists(filepath))
            throw new FileReadException(FileReadExceptionType.Csproj, filepath, parent.Filepath);

        using var projectCollection = new ProjectCollection();
        var msbuildProject = new Microsoft.Build.Evaluation.Project(filepath, new Dictionary<string, string>(), null, projectCollection, ProjectLoadSettings.IgnoreInvalidImports | ProjectLoadSettings.IgnoreMissingImports);
        var usingSdk = msbuildProject.Properties.FirstOrDefault(prop => prop.Name == "UsingMicrosoftNETSdk")?.EvaluatedValue.Equals("true", StringComparison.InvariantCultureIgnoreCase) ?? false;
        var targetFrameworks = Array.Empty<string>();

        if (usingSdk)
        {
            targetFrameworks = msbuildProject.GetProperty("TargetFrameworks") switch
            {
                ProjectProperty pp => pp.EvaluatedValue.Split(';', StringSplitOptions.RemoveEmptyEntries).Select(val => val.Trim()).ToArray(),
                null => new[] { msbuildProject.GetPropertyValue("TargetFramework") }
            };
        }
        else
        {
            targetFrameworks = new[] {
                msbuildProject.GetPropertyValue("TargetFrameworkVersion")
                              .Replace("v", "net")
                              .Replace(".", "")
            };
        }

        var packageReferences = new Dictionary<string, Reference>();
        var projectReferences = new Dictionary<string, Reference>();
        var frameworkReferences = new Dictionary<string, Reference>();

        foreach(var targetFramework in targetFrameworks)
        {
            msbuildProject.SetGlobalProperty("TargetFramework", targetFramework);
            msbuildProject.ReevaluateIfNecessary();

            foreach (var include in GetItems(msbuildProject, "PackageReference"))
            {
                if (!packageReferences.TryGetValue(include, out var reference))
                {
                    reference = new Reference(include);
                    packageReferences.Add(include, reference);
                }

                reference.Frameworks.Add(targetFramework);
            }

            foreach (var item in msbuildProject.GetItems("ProjectReference"))
            {
                var include = ConvertPathSeparators(item.EvaluatedInclude);

                if (!projectReferences.TryGetValue(include, out var reference))
                {
                    reference = new Reference(include)
                    {
                        Origin = item.GetMetadataValue("Origin")
                    };
                    projectReferences.Add(include, reference);
                }

                reference.Frameworks.Add(targetFramework);
            }

            foreach (var item in msbuildProject.GetItems("Reference"))
            {
                if (item.Metadata.Any(meta => meta.Name == "IsImplicitlyDefined" && meta.EvaluatedValue.Equals("true", StringComparison.InvariantCultureIgnoreCase)) ||
                    item.EvaluatedInclude == "mscorlib")
                {
                    continue;
                }

                if (!frameworkReferences.TryGetValue(item.EvaluatedInclude, out var reference))
                {
                    reference = new Reference(item.EvaluatedInclude)
                    {
                        Origin = item.GetMetadataValue("Origin")
                    };
                    frameworkReferences.Add(item.EvaluatedInclude, reference);

                    outputWriter.PrintTrace("Adding framework reference '{0}' for {1} with metadata: {2}", item.EvaluatedInclude, filepath, string.Join("; ", item.Metadata.Select(meta => $"\"{meta.Name}\"=\"{meta.EvaluatedValue}\"")));
                }

                reference.Frameworks.Add(targetFramework);
            }
        }

        var packageId = await GetPackageId(filepath, msbuildProject);

        projectCollection.UnloadAllProjects();
        return new Project(filepath, packageId, packageReferences.Values.ToList(), projectReferences.Values.ToList(), frameworkReferences.Values.ToList(), targetFrameworks, outputWriter, !usingSdk);
    }

    internal static Project CreateForTesting(string filepath,
                                             string packageId)
        => new(filepath, packageId, Array.Empty<Reference>(), Array.Empty<Reference>(), Array.Empty<Reference>(), Array.Empty<string>(), new TestOutputWriter(), false);

    public IList<Reference> GetUnresolvedPackageReferences(Workspace workspace)
    {
        var projectReferences = ProjectReferences.Join(workspace.PackageLookup.Values,
                                                       reference => Path.GetFullPath(reference.Include, Path.GetDirectoryName(Filepath)!),
                                                       proj => proj.Filepath,
                                                       (reference, proj) => new KeyValuePair<string, Reference>(proj.PackageId, reference))
                                                 .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
        return PackageReferences.Select(reference =>
            {
                if (projectReferences.TryGetValue(reference.Include, out var projectReference) &&
                    projectReference.Frameworks.OrderBy(f => f).SequenceEqual(reference.Frameworks.OrderBy(f => f)))
                {
                    return null;
                }

                return new Reference(reference.Include)
                {
                    Frameworks = projectReference is not null
                        ? reference.Frameworks.Except(reference.Frameworks.Intersect(projectReference.Frameworks)).ToList()
                        : reference.Frameworks
                };
            })
            .WhereNotNull()
            .ToList();
    }

    public void AddReferences(Workspace workspace)
        => GetUnresolvedPackageReferences(workspace).Where(reference => workspace.PackageLookup.ContainsKey(reference.Include))
                                                    .ForEach(reference => reference.Frameworks.ForEach(framework =>
                                                        Changes.Add(new ProjectChange(workspace.PackageLookup[reference.Include], framework, ChangeType.Added))));

    public void AddTransitiveReferences(Workspace workspace, RunnerOptions options)
    {
        var graph = NugetGraph.Create(this, options.NoRestore, _outputWriter);
        var transitiveFrameworkReferences = new List<Reference>();

        foreach (var newReference in graph.GetTransitiveReferencedProjects(workspace, IsLegacy, _outputWriter))
        {
            foreach (var framework in newReference.Frameworks)
            {
                var projectReferences = GetProjectReferences(workspace, new[] { framework });
                if (projectReferences.Any(r => r.PackageId == newReference.Include))
                {
                    continue;
                }

                var project = workspace.PackageLookup[newReference.Include];
                Changes.Add(new ProjectChange(project, framework, ChangeType.Added));

                transitiveFrameworkReferences.AddRange(project.FrameworkReferences);
            }
        }
    }

    public void AddFrameworkReferences(Workspace workspace)
    {
        var frameworkReferenes = GetProjectReferences(workspace).SelectMany(proj => proj.FrameworkReferences.SelectMany(pr => pr.Frameworks.Select(framework => (reference: pr, framework))))
                                                                .DistinctBy(t => (t.reference.Include, t.framework))
                                                                .Where(t => TargetFrameworks.Contains(t.framework))
                                                                .ToList();
        foreach (var reference in frameworkReferenes)
        {
            if (!FrameworkReferences.Any(r => r.Include == reference.reference.Include && r.Frameworks.Contains(reference.framework)))
            {
                Changes.Add(new ReferenceChange(reference.reference, reference.framework, ChangeType.Added));
                _outputWriter.PrintTrace("{0}: Added framework reference '{1}'", Filepath, reference.reference.Include);
            }
        }
    }

    public void RemoveReferences(Workspace workspace)
        => ProjectReferences.Where(reference => reference.Origin == "slnmerge")
                            .SelectMany(reference => reference.Frameworks.Select(framework => (reference, framework)))
                            .ForEach(tuple => Changes.Add(new ProjectChange(workspace.PackageLookup.Values.First(p => p.Filepath == Path.GetFullPath(tuple.reference.Include, Path.GetDirectoryName(Filepath)!)), tuple.framework, ChangeType.Removed)));

    public void RemoveFrameworkReferences()
        => FrameworkReferences.Where(reference => reference.Origin == "slnmerge")
                              .SelectMany(reference => reference.Frameworks.Select(framework => (reference, framework)))
                              .ForEach(t => Changes.Add(new ReferenceChange(t.reference, t.framework, ChangeType.Removed)));

    public void WriteChanges()
    {
        using var projectCollection = new ProjectCollection();
        var project = ProjectRootElement.Open(Filepath, projectCollection, true);
        var projectChanges = Changes.Where(change => change.ChangeType == ChangeType.Added || change.ChangeType == ChangeType.Removed)
                                    .OfType<ProjectChange>()
                                    .GroupBy(change => change.Project)
                                    .Select(change => TargetFrameworks.All(tf => change.Any(c => c.Framework == tf))
                                        ? (change.Key, frameworks: _arrayOfEmptyString) 
                                        : (change.Key, frameworks: change.Select(c => c.Framework)))
                                    .SelectMany(tuple => tuple.frameworks.Select(framework => (tuple.Key, framework)));
        var projectChangeTypeDict = Changes.Where(change => change.ChangeType == ChangeType.Added || change.ChangeType == ChangeType.Removed)
                                           .OfType<ProjectChange>()
                                           .DistinctBy(change => change.Project)
                                           .ToDictionary(c => c.Project, c => c.ChangeType);

        foreach (var (addedProject, framework) in projectChanges.Where(c => projectChangeTypeDict.TryGetValue(c.Key, out var ct) && ct == ChangeType.Added))
        {
            var itemGroup = project.ItemGroups.FirstOrDefault(ig => IsConditionForFramework(ig.Condition, framework) && DoesItemGroupContainProjectReference(ig));

            if (itemGroup == null)
            {
                itemGroup = project.AddItemGroup();

                if (framework != string.Empty)
                {
                    itemGroup.Condition = $"'$(TargetFramework)' == '{framework}'";
                }
            }

            var item = itemGroup.AddItem("ProjectReference", Path.GetRelativePath(Path.GetDirectoryName(Filepath)!, addedProject.Filepath));
            item.AddMetadata("Origin", "slnmerge", true);
        }

        foreach (var (removedProject, _) in projectChanges.Where(c => projectChangeTypeDict[c.Key] == ChangeType.Removed))
        {
            var item = project.ItemGroups
                              .SelectMany(ig => ig.Items)
                              .FirstOrDefault(item => item.ElementName == "ProjectReference"
                                                      && Path.GetFullPath(ConvertPathSeparators(item.Include), Path.GetDirectoryName(Filepath)!) == removedProject.Filepath);

            if (item == null)
            {
                _outputWriter.PrintWarning(new Exception($"Could not find ProjectReference in {Filepath} to remove for {removedProject.Filepath}"));
                continue;
            }

            if (item.Parent is ProjectElementContainer group)
            {
                group.RemoveChild(item);

                if (group.Children.Count == 0)
                {
                    group.Parent.RemoveChild(group);
                }
            }
            else
            {
                _outputWriter.PrintWarning(new Exception($"ProjectReference for {item.Include} is not in an ItemGroup in {Filepath}"));
            } 
        }

        var referenceChanges = Changes.Where(change => change.ChangeType == ChangeType.Added || change.ChangeType == ChangeType.Removed)
                                      .OfType<ReferenceChange>()
                                      .GroupBy(change => change.Reference.Include)
                                      .Select(change => TargetFrameworks.All(tf => change.Any(c => c.Framework == tf))
                                          ? (change.Key, frameworks: _arrayOfEmptyString)
                                          : (change.Key, frameworks: change.Select(c => c.Framework)))
                                      .SelectMany(tuple => tuple.frameworks.Select(framework => (tuple.Key, framework)));
        var referenceChangeTypeDict = Changes.Where(change => change.ChangeType == ChangeType.Added || change.ChangeType == ChangeType.Removed)
                                             .OfType<ReferenceChange>()
                                             .DistinctBy(change => change.Reference.Include)
                                             .ToDictionary(c => c.Reference.Include, c => c.ChangeType);

        foreach (var (addedReference, framework) in referenceChanges.Where(c => referenceChangeTypeDict.TryGetValue(c.Key, out var ct) && ct == ChangeType.Added))
        {
            var itemGroup = project.ItemGroups.FirstOrDefault(ig => IsConditionForFramework(ig.Condition, framework) && DoesItemGroupContainFrameworkReference(ig));

            if (itemGroup == null)
            {
                itemGroup = project.AddItemGroup();

                if (framework != string.Empty)
                {
                    itemGroup.Condition = $"'$(TargetFramework)' == '{framework}'";
                }
            }

            var item = itemGroup.AddItem("Reference", addedReference);
            item.AddMetadata("Origin", "slnmerge", true);
        }

        foreach (var (removedReference, _) in referenceChanges.Where(c => referenceChangeTypeDict.TryGetValue(c.Key, out var ct) && ct == ChangeType.Removed))
        {
            var items = project.ItemGroups
                               .SelectMany(ig => ig.Items)
                               .Where(item => item.ElementName == "Reference"
                                              && item.Include == removedReference);

            if (!items.Any())
            {
                _outputWriter.PrintWarning(new Exception($"Could not find Reference in {Filepath} to remove for {removedReference}"));
                continue;
            }

            foreach(var item in items)
            {
                if (item.Parent is ProjectElementContainer group)
                {
                    group.RemoveChild(item);

                    if (group.Children.Count == 0)
                    {
                        group.Parent.RemoveChild(group);
                    }
                }
                else
                {
                    _outputWriter.PrintWarning(new Exception($"Reference for {item.Include} is not in an ItemGroup in {Filepath}"));
                }
            }
        }

        if (Changes.OfType<VersionChange>().Any(change => change.ChangeType == ChangeType.Added))
        {
            var versions = project.Properties.Where(prop => prop.Name == "Version").ToList();
            var needsGenerated = true;

            foreach (var version in versions)
            {
                if (version.Condition?.Contains("slnmerge") ?? false)
                {
                    if (version.Condition.Contains("Generated"))
                    {
                        needsGenerated = false;
                    }
                    continue;
                }

                if (string.IsNullOrEmpty(version.Condition))
                {
                    version.Condition = "'Original'=='slnmerge'";
                }
                else
                {
                    version.Condition = $"'Original'=='slnmerge'&&{version.Condition}";
                }
            }

            if (needsGenerated)
            {
                var generatedVersion = project.AddProperty("Version", "9999.0.0");
                generatedVersion.Condition = "'Generated'!='slnmerge'";
            }
        }

        if (Changes.OfType<VersionChange>().Any(change => change.ChangeType == ChangeType.Removed))
        {
            var versions = project.Properties.Where(prop => prop.Name == "Version").ToList();

            foreach (var version in versions)
            {
                if (version.Condition?.Contains("slnmerge") ?? false)
                {
                    if (version.Condition.Contains("'Generated'!='slnmerge'"))
                    {
                        version.Parent.RemoveChild(version);
                        continue;
                    }
                    if (version.Condition.Contains("'Original'=='slnmerge'"))
                    {
                        version.Condition = Regex.Replace(version.Condition, "'Original'=='slnmerge'(&&)?", "");
                    }
                }
            }
        }

        project.Save();

        bool IsConditionForFramework(string condition, string framework)
            => framework == string.Empty && condition == string.Empty ||
               Regex.IsMatch(condition, $"'\\$\\(TargetFramework\\)'\\s*==\\s*'{framework}'");

        bool DoesItemGroupContainProjectReference(ProjectItemGroupElement itemGroup)
            => itemGroup.Children.Any(item => item.ElementName == "ProjectReference");

        bool DoesItemGroupContainFrameworkReference(ProjectItemGroupElement itemGroup)
            => itemGroup.Children.Any(item => item.ElementName == "Reference");
    }

    private static List<string> GetItems(Microsoft.Build.Evaluation.Project msbuildProject, string itemType)
        => msbuildProject.GetItems(itemType)
                         .Select(item => item.EvaluatedInclude)
                         .ToList();

    private static async Task<string> GetPackageId(string filepath, Microsoft.Build.Evaluation.Project msbuildProject)
    {
        try
        {
            var nuspec = await XDocument.LoadAsync(File.OpenRead(Path.ChangeExtension(filepath, "nuspec")), LoadOptions.None, default);
            var namespaceManager = new XmlNamespaceManager(nuspec.CreateNavigator().NameTable);
            namespaceManager.AddNamespace("nuspec", "http://schemas.microsoft.com/packaging/2010/07/nuspec.xsd");

            var idElement = nuspec?.Root?.XPathSelectElement("/nuspec:package/nuspec:metadata/nuspec:id", namespaceManager);

            if (idElement != null)
            {
                return idElement.Value;
            }
        }
        catch (FileNotFoundException)
        {
            // ignore
        }

        var nuspecFile = msbuildProject.GetPropertyValue("NuspecFile");
        var nuspecPath = Path.Combine(Path.GetDirectoryName(filepath)!, nuspecFile);

        if (!string.IsNullOrEmpty(nuspecFile) && !File.Exists(nuspecPath))
        {
            throw new FileReadException(FileReadExceptionType.Nuspec, nuspecPath, filepath);
        }

        var packageId = msbuildProject.GetPropertyValue("PackageId");

        if (!string.IsNullOrEmpty(packageId))
        {
            return packageId;
        }

        return Path.GetFileNameWithoutExtension(filepath);
    }

    public async Task PopulateSolutionAsync(Solution parent, Workspace workspace)
    {
        Console.WriteLine($"Checking {Path.GetFileName(parent.Filepath)} for {PackageId}");
        if (!(await parent.Projects).Contains(this))
        {
            parent.Changes[this] = ChangeType.Added;
        }

        await ProjectReferences.Select(reference => Path.GetFullPath(reference.Include, Path.GetDirectoryName(Filepath)!))
                               .Join(workspace.PackageLookup.Values, path => path, proj => proj.Filepath, (path, proj) => proj)
                               .Select(proj => proj.PopulateSolutionAsync(parent, workspace))
                               .WhenAll();
    }

    public async Task AddVersionOverrideAsync(Workspace workspace)
    {
        var projects = await workspace.Solutions.Select(sln => sln.Projects.Value)
                                                .WhenAll(projs => projs.SelectMany(projs => projs))
                                                .ToListAsync();
        var isReferenced = projects.Any(proj => proj.PackageReferences.Select(reference => (reference, path: Path.GetFullPath(reference.Include, Path.GetDirectoryName(Filepath)!)))
                                                                      .Join(workspace.PackageLookup.Values, tuple => tuple.path, proj => proj.Filepath, (tuple, proj) => (tuple.reference, proj))
                                                                      .Select(tuple => tuple.proj)
                                                                      .Concat(proj.Changes.Where(change => change.ChangeType == ChangeType.Added)
                                                                                          .OfType<ProjectChange>()
                                                                                          .Select(kvp => kvp.Project))
                                                                      .Any(p => p == this));

        if (isReferenced)
        {
            using var projectCollection = new ProjectCollection();
            var msbuildProject = ProjectRootElement.Open(Filepath, projectCollection, true);
            var versions = msbuildProject.Properties.Where(prop => prop.Name == "Version").ToList();

            if (!versions.Any(version => version.Condition?.Contains("slnmerge") ?? false))
            {
                Changes.Add(new VersionChange(ChangeType.Added));
            }
        }
    }

    public Task RemoveVersionOverrideAsync()
    {
        Changes.Add(new VersionChange(ChangeType.Removed));
        return Task.CompletedTask;
    }

    public IEnumerable<Project> GetProjectReferences(Workspace workspace, ICollection<string>? frameworks = null)
        => ProjectReferences.Where(reference => frameworks == null || reference.Frameworks.Intersect(frameworks).Any())
                            .Select(reference => (reference, path: Path.GetFullPath(reference.Include, Path.GetDirectoryName(Filepath)!)))
                            .Join(workspace.PackageLookup.Values, tuple => tuple.path, proj => proj.Filepath, (tuple, proj) => (tuple.reference, proj))
                            .Where(tuple => tuple.reference.Frameworks.Any(fw => !Changes.OfType<ProjectChange>().Any(c => c.ChangeType == ChangeType.Removed && c.Project == tuple.proj && fw == c.Framework)))
                            .Select(tuple => tuple.proj)
                            .Concat(Changes.Where(change => change.ChangeType == ChangeType.Added)
                                           .OfType<ProjectChange>()
                                           .Where(change => frameworks == null || frameworks.Contains(change.Framework))
                                           .Select(kvp => kvp.Project))
                            .Distinct();

    public override string ToString()
        => $"Project({Filepath})";

    private static string ConvertPathSeparators(string path)
        => Regex.Replace(path, "[/\\\\]", Path.DirectorySeparatorChar.ToString());
}

internal class TestOutputWriter : IOutputWriter
{
    public TraceLevel LogLevel { get; set; }

    public void PrintArgumentMessage(string message) { }
    public void PrintCommand(string command) { }
    public void PrintComplete(int numModified) { }
    public void PrintException(Exception exception) { }
    public void PrintInfo(string message) { }
    public void PrintProgress(string file) { }
    public void PrintTrace(string format, params object[] args) { }
    public void PrintWarning(Exception exception) { }
    public Task StartProgressContext(RunnerOptions options, Func<IProgressContext, Task> predicate) => Task.CompletedTask;
}