using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace AHelper.SlnMerge.Core;

internal static class EnumerableExtensions
{
    public static async Task<IEnumerable<T>> WhenAll<T>(this IEnumerable<Task<T>> enumerable)
        => await Task.WhenAll(enumerable);

    public static Task WhenAll(this IEnumerable<Task> enumerable)
        => Task.WhenAll(enumerable);

    public static async Task WhenAll(this Task<IEnumerable<Task>> enumerable)
        => await Task.WhenAll(await enumerable);

    public static Task WhenAll<T>(this IEnumerable<Task<T>> enumerable, Action<IEnumerable<T>> predicate)
        => Task.WhenAll(enumerable).ContinueWith(task => predicate(task.Result));

    public static Task<TOut> WhenAll<TIn, TOut>(this IEnumerable<Task<TIn>> enumerable, Func<IEnumerable<TIn>, TOut> predicate)
        => Task.WhenAll(enumerable).ContinueWith(task => predicate(task.Result));

    public static IEnumerable<T> WhereNotNull<T>(this IEnumerable<T?> enumerable)
        => (IEnumerable<T>)enumerable.Where(value => value is not null);

    public static Task<List<T>> ToListAsync<T>(this Task<IEnumerable<T>> enumerable)
        => enumerable.ContinueWith(task => task.Result.ToList());

    public static Task<Dictionary<TKey, TValue>> ToDictionaryAsync<T, TKey, TValue>(this Task<IEnumerable<T>> enumerable, Func<T, TKey> keyPredicate, Func<T, TValue> valuePredicate) where TKey : notnull
        => enumerable.ContinueWith(task => task.Result.ToDictionary(keyPredicate, valuePredicate));

    public delegate bool TrySelectPredicate<TIn, TOut>(TIn value, [MaybeNullWhen(false)] out TOut output);
    public static IEnumerable<TOut> TrySelect<TIn, TOut>(this IEnumerable<TIn> enumerable, TrySelectPredicate<TIn, TOut> predicate)
    {
        foreach (var item in enumerable)
        {
            if (predicate(item, out var output))
            {
                yield return output;
            }
        }
    }

    public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> enumerable, Func<T, TKey> predicate)
    {
        var keys = new HashSet<TKey>();

        foreach (var item in enumerable)
        {
            var key = predicate(item);
            if (keys.Add(key))
            {
                yield return item;
            }
        }
    }

    public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> predicate)
    {
        foreach (var element in enumerable)
        {
            predicate(element);
        }
    }

    public static IEnumerable<T> Expand<T>(this T? item, Func<T, T?> predicate)
    {
        while (item != null)
        {
            yield return item;
            item = predicate(item);
        }
    }
}
