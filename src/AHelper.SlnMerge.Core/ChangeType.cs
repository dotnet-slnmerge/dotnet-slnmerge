namespace AHelper.SlnMerge.Core;

public enum ChangeType
{
    Added,
    Removed
}
