﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using AHelper.SlnMerge;
using AHelper.SlnMerge.Core;
using Flurl.Http;
using Semver;

var metadataPath = Path.Join(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "slnmerge", "metadata.json");
var outputWriter = new SpectreConsoleOutputWriter();
await CheckForUpdatesAsync(outputWriter);
await new Runner(outputWriter).RunAsync(args);

async Task CheckForUpdatesAsync(SpectreConsoleOutputWriter outputWriter)
{
    try
    {
        Directory.CreateDirectory(Path.GetDirectoryName(metadataPath)!);
        if (File.Exists(metadataPath))
        {
            var metadata = JsonSerializer.Deserialize<Metadata>(await File.ReadAllTextAsync(metadataPath));

            if (metadata is not null && DateTime.UtcNow - metadata.LastUpdateCheckTime >= Metadata.UpdateCheckInterval)
            {
                metadata.LastUpdateCheckTime = DateTime.UtcNow;
                await File.WriteAllTextAsync(metadataPath, JsonSerializer.Serialize(metadata));

                var response = await "https://gitlab.com/api/v4/projects/24002513/releases".WithTimeout(TimeSpan.FromSeconds(5))
                                                                                           .GetJsonAsync<List<ReleaseResponse>>();
                var latestVersion = response.Select(release => release.Name)
                                            .Select(name => name.TrimStart('v', 'V'))
                                            .Select(name => SemVersion.Parse(name, SemVersionStyles.Any))
                                            .OrderByDescending(version => version)
                                            .FirstOrDefault();
                var currentVersion = FileVersionInfo.GetVersionInfo(typeof(Program).Assembly.Location);

                if (currentVersion.ProductVersion != null && 
                    latestVersion != null && 
                    latestVersion != SemVersion.Parse(currentVersion.ProductVersion, SemVersionStyles.Any))
                {
                    outputWriter.PrintInfo($"A new version of dotnet-slnmerge was released ({latestVersion}). To update: dotnet tool update -g dotnet-slnmerge");
                }
            }
        }
        else
        {
            var metadata = new Metadata
            {
                LastUpdateCheckTime = DateTime.MinValue
            };

            await File.WriteAllTextAsync(metadataPath, JsonSerializer.Serialize(metadata));
        }
    }
    catch
    {
        // Ignore all errors
    }
}

class ReleaseResponse
{
    [JsonPropertyName("name")]
    [JsonRequired]
    public string Name { get; set; } = null!;
}